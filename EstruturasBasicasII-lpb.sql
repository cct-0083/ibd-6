﻿-- Este procedimento utiliza um rótulo <<main>> para que a instrução EXIT
-- tenha referência de qual bloco ela se refere

CREATE OR REPLACE FUNCTION EH_PAR_IMPAR(NUMERO INTEGER) RETURNS VARCHAR AS $$
<<main>>
BEGIN
  -- PERFORM CHAMA OUTRA FUNCAO
  PERFORM PRINT('INICIANDO A AVALIAÇÃO...');
  IF (NUMERO IS NULL) THEN
     RETURN 'ERRO';
     EXIT main;
  ELSEIF ((NUMERO % 2) = 0) THEN
     RETURN 'PAR';
  ELSE
     RETURN 'IMPAR';
  END IF;
  
  PERFORM PRINT('FINALIZANDO A AVALIAÇÃO...');
  RETURN 'FIM';
END;
$$ LANGUAGE plpgsql;

-- Usando a função Eh_PAR_IMPAR

select eh_par_impar(4);

-- Estrutura case..when
CREATE OR REPLACE FUNCTION TRIMESTRE(DATA_REF DATE) RETURNS VARCHAR AS $$
DECLARE
   MES INTEGER:=0; 
BEGIN
   MES := EXTRACT(MONTH FROM DATA_REF);
   CASE
     WHEN MES IN (1,2,3) THEN RETURN '1º TRIMESTRE';
     WHEN MES IN (4,5,6) THEN RETURN '2º TRIMESTRE';
     WHEN MES IN (7,8,9) THEN RETURN '3º TRIMESTRE';
     WHEN MES IN (10,11,12) THEN RETURN '4º TRIMESTRE';
   END CASE;
END   
$$ LANGUAGE PLPGSQL

-- Outra versão da mesma função
CREATE OR REPLACE FUNCTION TRIMESTRE(DATA_REF DATE) RETURNS VARCHAR AS $$
BEGIN
  CASE EXTRACT(QUARTER FROM DATA_REF)
    WHEN 1 THEN RETURN '1º TRIMESTRE';
    WHEN 2 THEN RETURN '2º TRIMESTRE';
    WHEN 3 THEN RETURN '3º TRIMESTRE';
    WHEN 4 THEN RETURN '4º TRIMESTRE';
  END CASE;  
END
$$ LANGUAGE PLPGSQL

-- Exemplo usando trimestre
SELECT TRIMESTRE(NOW()::DATE);

-- Estrutura case..when
CREATE OR REPLACE FUNCTION AVALIAR_SALARIO(VALOR NUMERIC) RETURNS VARCHAR AS $$
BEGIN
  CASE 
    WHEN (VALOR BETWEEN 0 AND 1500) THEN RETURN 'BAIXO';
    WHEN (VALOR BETWEEN 1501 AND 3000) THEN RETURN 'MEDIO';
    ELSE RETURN 'ALTO'; 
  END CASE;
END
$$ LANGUAGE PLPGSQL

-- Exemplo do uso da função avaliar_salario
SELECT AVALIAR_SALARIO(3500.70)

-- USANDO A SAIDA DE UMA FUNÇÃO EM OUTRA
CREATE OR REPLACE FUNCTION TESTE_CHAMADA() RETURNS VARCHAR AS $$
DECLARE
   SAL VARCHAR :='';
BEGIN
   -- PERFORM DESCONSIDERA O SELECT
   SELECT INTO SAL AVALIAR_SALARIO(3000);
   -- OU TAMBEM
   -- SELECT AVALIAR_SALARIO(3000) INTO SAL;
   RETURN SAL;
END
$$ LANGUAGE PLPGSQL

SELECT TESTE_CHAMADA();

SELECT TRIMESTRE('2019-02-25');