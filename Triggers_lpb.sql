-- Exemplo usando triggers para postgresSQL
-- Crie a tabela funcionario

create table funcionario (
  codigo int,
  nome varchar(30),
  salario real,
  depto varchar(30),
  constraint "funcionario pk" primary key(codigo));
  
-- O postgres trabalha com o conceito de função trigger. Se trata de uma
função comum com alguns requisitos e que posteriormente é associada a uma
trigger

create table tarefa (
  id int,
  descricao varchar(30),
  func_id int,
  constraint "tarefa_pk" primary key(id),
  constraint "tarefa_fk" foreign key (func_id) references funcionario);
  
-- Usaremos essas duas tabelas para verificar o funcionamento das triggers.

-- A primeira trigger adiciona uma tarefa inicial para cada funcionário cadastrado

create sequence seq_tarefa_id; 

-- Uma função trigger (gatilho) não pode passar nenhum parametro e deve retornar trigger

create or replace function trigger_insert_funcionario() returns trigger as $body$
begin
	-- A funcao trigger descreve o que fazer depois de um evento
	-- Neste caso ela adiciona uma tarefa chamada apresentacao para cada funcionário novo
	insert into tarefa values (nextval('seq_tarefa_id'),'Apresentação...',new.codigo);
	return new;
end
$body$ language plpgsql;

-- Aqui a trigger propriamente dita
create trigger tarefa_trigger after insert on funcionario
for each row execute procedure trigger_insert_funcionario();

- Inserindo um novo funcionario

insert into funcionario values (1,'Alessandra',3525.75,'RH');

-- Funcionário foi inserido com sucesso... 
-- e a tarefa apresentacao foi inserida para a alessandra.

select * from tarefa;

-- Inserindo mais funcionarios
insert into funcionario values (2,'Ricardo',8729.78,'TI');
insert into funcionario values (3,'Cinthia',12872.77,'SUPORTE');
insert into funcionario values (4,'Nelton',15250.00,'AD');

-- Verifiquem o resultado das tarefas

select * from tarefa;

-- Agora vamos criar uma trigger para apagar todas as tarefas na ocasião de um funcionario ser demitido
create or replace function trigger_delete_funcionario() returns trigger as $body$
begin
	delete from tarefa where func_id=old.codigo;
	return old;
end
$body$ language plpgsql;

create trigger delete_tarefa before delete on funcionario
for each row execute procedure trigger_delete_funcionario();

-- Agora vamos apagar um funcionário

delete from funcionario where codigo=1;

-- Apagamos o registro para o funcionario Alessandra!!
-- Verifique se existe tarefa relacionada para ela:

select * from tarefa 


-- Apagando uma trigger (exemplo de delete_tarefa em funcionario);
drop trigger delete_tarefa on funcionario;

-- No último exemplo, lançar uma nova tarefa se o funcionario mudar de departamento
create or replace function change_funcionario() returns trigger as $body$
begin
   -- Só lança uma nova tarefa se houver mudança de departamento	
   if (old.depto <> new.depto) then
	insert into tarefa values(nextval('seq_tarefa_id'),'Reapresentação...',new.codigo);
   end if;
   return new;
end
$body$ language plpgsql

create trigger update_funcionario_depto after update on funcionario for each row
execute procedure change_funcionario();

-- Mudando a funcionaria cinthia de SUPORTE para AD

update funcionario set depto = 'AD' where nome='Cinthia';

-- Verifique se houve algum lançamento novo lançamento para a Cinthia

select * from tarefa




-- Tabela que terá a inclusão, exclusão e atualização
-- e registrará estas informações na tabela de auditoria
drop table if exists cliente_teste;
create table cliente_teste (
	codigo serial,
	nome varchar(30) not null,
	primary key (codigo)
)
-- Usando operações especiais de auditoria com triggers
drop table if exists auditoria;
create table auditoria (
	operacao varchar(30) not null,
	user_id text not null,
	table_name name not null, -- Consultar o tipo de dado no postgres
	data date
)

-- Criação da função de trigger
create or replace function audit_log() returns trigger as $body$
 begin
	if (tg_op = 'DELETE') then
	   insert into auditoria select 'Delete', user, tg_table_name, now();
	   return old;		
	end if;
	if (tg_op = 'UPDATE') then
	   insert into auditoria select 'Update', user, tg_table_name, now();
	   return new;     	
	end if;
	if (tg_op = 'INSERT') then
	   insert into auditoria select 'Insert', user, tg_table_name, now();
	   return new;	
	end if;
	return null;
 end;
$body$ language plpgsql;

create trigger trigger_auditoria after insert or update or delete on cliente_teste
for each row execute procedure audit_log();

-- Manipulando a tabela de cliente_test;
insert into cliente_teste(nome) values ('Aline'),('Luana'),('Wellington');

update cliente_teste
set nome = 'Bernardo'
where nome = 'Wellington'

delete from cliente_teste where nome = 'Luana';

-- Agora selecionando tudo o que tem na tabela de auditoria
select * from cliente_teste
select * from auditoria;
