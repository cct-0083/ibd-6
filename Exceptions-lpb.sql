-- Exemplos de exceptions

create or replace function divisao(divisor integer, dividendo integer) returns real as $body$
declare res real;
begin
  begin
  	res = (divisor/dividendo);
  exception 
  	when division_by_zero then
  		res = -1;
  end;
  return res;
end;
$body$ language plpgsql;

select divisao(1,0)