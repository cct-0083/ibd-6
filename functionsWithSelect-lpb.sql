create or replace function select_nome_vendedores() returns setof record as $$
begin
  -- Esta consulta retorna a coluna nome existente na tabela vendedor
  return query select nome from vendedor
  return;
end
$$ language plpgsql;

select * from select_nome_vendedores() as (nome varchar);

-- A consulta a seguir retorna todos os campos

create or replace function todos_vendedores() returns setof vendedor as $$
begin
  -- Esta consulta retorna as colunas existentes na tabela vendedor
  return query select * from vendedor
  return;
end
$$ language plpgsql;

select * from todos_vendedores();

-- Retornando somente a coluna referente ao nome do vendedor
create or replace function nomes_vendedor() returns table (nome_vendedor varchar(30)) as $$
begin
    return query select nome from vendedor
    return;
end
$$ language 'plpgsql';

select * from nomes_vendedor();

-- Outra forma de fazer isso é criando um tipo (type)
create type nome_vendedor as (nome varchar(30));

create or replace function mostra_nome_vendedores() returns setof nome_vendedor as $$
declare nome_rec nome_vendedor;

begin
  for nome_rec in select nome from vendedor loop  -- uso do for com loop
     return next nome_rec;
  end loop;
  return;   
end
$$
language plpgsql;

select * from mostra_nome_vendedores();

-- Criar table cliente e produtos com a seguinte estrutura
create table cliente (
 id serial,
 nome varchar(30),
 constraint "cliente_pk" primary key (id));
 
create table produtos (
 id serial,
 descricao varchar(30),
 valor numeric,
 cliente_id int,
 constraint "produtos_pk" primary key (id),
 constraint "produtos_fk" foreign key (cliente_id) references cliente) 

-- Outro exemplo de consulta com query
create or replace function seleciona_cliente_produto() returns table(nome varchar, descricao varchar) as $body$
begin
 return query select c.nome, p.descricao from cliente c join produtos p on (c.id = p.cliente_id); 
 return;
end
$body$ language plpgsql

select * from seleciona_cliente_produto();

--Exemplo usando setof (grupo de varchar)
drop function mostra_um_cliente(integer);
create or replace function mostra_um_cliente(ident integer) returns setof varchar as $$
declare nome_cliente varchar;
begin
 for nome_cliente in select nome from cliente loop
   raise notice 'O nome do cliente até este momento é %',nome_cliente;
   return next nome_cliente;
 end loop;
 return;
end
$$ language plpgsql

select * from mostra_um_cliente(4);

-- Ao usar record não se determina a quantidade de campos nem os tipos
create or replace function mostra_clientes() returns setof record as $teste$
begin
	return query select id, nome from cliente;
	return;
end
$teste$ language plpgsql

--A consulta com a função usando record precisa indicar os campos e os tipos;
select * from mostra_clientes() as (id integer, nome varchar);

-- funcao que retorna uma table como resultado.
create or replace function mostra_clientes_table() returns table (ident int, nome_clie varchar) as $body$
declare codigo int;
        nome_cliente varchar;
begin
   for codigo,nome_cliente in select id,nome from cliente loop
	ident :=codigo;
	nome_clie:=nome_cliente;
	return next;
   end loop;
   return;
end
$body$ language plpgsql

select * from  mostra_clientes_table()
