-- Tabelas usadas para o exemplo
drop table if exists compra_venda;

drop table if exists vendedor;
create table vendedor (
  codigo serial,
  cpf char(11),
  nome varchar(30),
  cidade varchar(60),
  primary key(codigo));
  
drop table if exists comprador;
create table comprador (
  codigo serial,
  cpf char(11),
  nome varchar(30),
  cidade varchar(60),
  primary key(codigo));

create table compra_venda(
  codigo_comprador int,
  codigo_vendedor int,
  valor numeric,
  constraint "compra_venda_pk" primary key (codigo_comprador,codigo_vendedor),
  constraint "comprador_fk" foreign key (codigo_comprador) references comprador,
  constraint "vendedor_fk" foreign key (codigo_vendedor) references vendedor
);

-- Uso de cursores implicito

create or replace function vende_e_compra() returns setof record as $$
declare testc refcursor;
begin
   for testc in select nome from vendedor where nome in (Select nome from comprador) loop
	if (testc is not null) then
		return next testc;
	end if;
   end loop;
   return;
end;
$$ language plpgsql;

select * from vende_e_compra() as (nome varchar(60));

-- Exemplo de uso de cursor explicito

create or replace function mesmo_comprador_vendedor() returns setof varchar as $corpo$
declare nomes cursor for select nome from vendedor where cpf in (select cpf from comprador);
declare retorno varchar;
begin
  open nomes;
  loop
    fetch nomes into retorno;
    if (retorno is not null) then
	raise notice 'Exibiu o valor %',retorno;
	return next retorno;	
    end if;	
    exit when not found;
  end loop;
  return;
end
$corpo$ language plpgsql;

select * from mesmo_comprador_vendedor();

--Outro exemplo usar a tabela pessoa com os seguintes campos
drop table if exists pessoa;
create table pessoa (
  codigo serial,
  nome varchar(60),
  salario numeric,
  primary key (codigo)
)

--Retorna o salário e o nome de cada pessoa.

create or replace function mostra_dados_cliente() returns table (nom varchar, sal numeric) as $body$
declare my_cursor cursor for select nome, salario from pessoa;
declare nomeI varchar;
declare salarioI numeric;

begin
  open my_cursor;
  loop
    fetch my_cursor into nomeI, salarioI; 
    exit when not found;
    if (nomeI is not null) then
        nom := nomeI;
        sal := salarioI;
	return next;
    end if;
  end loop;
  close my_cursor;
  return;
end
$body$ language plpgsql

select * from mostra_dados_cliente();

--Exemplo de uso de cursores
create or replace function mostra_clientes_cursor() returns setof varchar as $teste$
declare manipulador cursor for select nome from cliente;
declare nome_res varchar;
begin
  open manipulador;
  loop
    fetch manipulador into nome_res;
    exit when not found;
    return next nome_res;
  end loop;
  close manipulador;
  return;
end
$teste$ language plpgsql