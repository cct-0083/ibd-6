﻿create table conta (
  id serial,
  correntista varchar,
  valor numeric,
  primary key (id));

-- Criação do indice garante unicidade do campo
create unique index idx_conta_correntista on conta(correntista);

insert into conta(correntista,valor) values('Joana',100.00),('Glauber',200.00);  

begin;
 update conta set valor=valor+100.00 where correntista='Joana';
commit;

--Houve a atualização, pois a transação foi efetivada.
select valor from conta where correntista='Joana';

begin;
 update conta set valor=valor+100.00 where correntista='Joana';
rollback;

-- Não houve atualização, pois a transação foi desfeita
select valor from conta where correntista='Joana';


-- Stored procedure with roolback control.
-- Ao dropar stored funcions, tem que passar os parâmetros também...
drop function if exists transferencia(correntista1 varchar, correntista2 varchar, vtransferencia numeric);
create or replace function transferencia(correntista1 varchar, correntista2 varchar, vtransferencia numeric) returns text as $body$
declare temp_value numeric;
declare exist_correntista1 varchar;
declare exist_correntista2 varchar;

begin
  begin
	  select valor into temp_value from conta where correntista = correntista1;
	  if (temp_value < vtransferencia) then
		return 'Saldo insuficiente';
	  elseif (temp_value >= vtransferencia and vtransferencia > 0) then
	     select correntista into exist_correntista1 from conta where correntista = correntista1; 	
	     select correntista into exist_correntista2 from conta where correntista = correntista2; 
	     if (exist_correntista1 is not null and exist_correntista2 is not null) then 		     
		     update conta set valor = valor - vtransferencia where correntista=correntista1;
		     update conta set valor = valor + vtransferencia where correntista=correntista2; 
		     return 'Transferencia de '|| vtransferencia ||' reais do correntista ' || correntista1 || 
		     ' para o correntista ' || correntista2;	
	     else 
		return 'Não foi encontrado um dos correntistas';
	     end if; 	     
	  else 
	     return 'Não foi possível transferir os valores';
	  end if;
  exception when others then
    raise exception 'Problemas internos...';
  end;
end
$body$ language plpgsql;

select transferencia('Joana','Lucia', 100.00);

-- Usando savepoint 
-- Ao ocorrer um roolback permite que o roolback volte até o rótulo do savepoint.
begin;
update conta set valor=valor + 100.00 where correntista='Joana';
savepoint save_1;
update conta set valor=valor - 100.00 where correntista='Glauber';
rollback to savepoint save_1;
commit;

select valor from conta where correntista='Joana';
 
-- Transações com concorrência.
-- Problemas com dirt read (leitura suja)
-- Ocorre quando uma transação lê um valor que foi modificado e a transação que modificou o valor sofre rollback, 
-- gerando inconsistencia
begin;
UPDATE conta SET correntista = 'Joana Darc' WHERE correntista='Joana';
SELECT * FROM conta;
rollback;
  
begin
--Vários comandos
UPDATE conta SET valor = 300 WHERE correntista = 'Joana';
commit;
-- como a transação ainda acha que a correntista joana existe, irá atualizar o saldo, gerando inconsistência.
-- o postgres não permite que esse tipo de situação ocorra.

-- Nonrepeatable read
-- Exemplo
--Transação A
begin;
UPDATE conta SET correntista = 'Joana Darc' WHERE correntista='Joana';
commit;
  
--Transação B
begin
update conta set valor = 350 WHERE correntista = 'Joana Darc';
commit;

-- Phanton read -> Similar ao repeatable read, porém, para os casos onde há inserção
--Transação A
begin;
insert into conta(correntista, valor) values ('Carlos',5000.00);
commit;
  
--Transação B
begin
SELECT * FROM teste.conta;
commit;


-- Se executada comitantemente a transação A, inicialmente não retorna nada, pois a primeira
-- transação não foi comitada. Neste caso não há como termos dirty read (leitura suja), pois 
-- nenhum valor está sendo atualizado.

/* Observações:
Dirty Read está relacionado com leitura de dados não commitados.
Nonrepeatable Read está relacionada com UPDATES.
Phanton Read está relacionado com DELETEs e INSERTS.
*/

-- O postgres não permite controlar o problema de Dirty Read.

/* Níveis de Isolamento
--  Read Uncommited; Read Commited; Repeatable Readed; Serializable;
Read Uncommited leva ao Dirty Read;
Read Commited é o padrão do postgresql e da maioria dos bancos de dados.
*/

--Repeated Read
begin;
 update conta set valor=2000.00 where correntista = 'Carlos';
commit;

begin;
set transaction isolation level repeatable read;
 select * from conta where correntista = 'Carlos';
commit;